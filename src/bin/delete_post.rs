use std::env::args;

use postgres::establish_connection;

use diesel::QueryDsl;
use diesel::RunQueryDsl;
use diesel::TextExpressionMethods;

fn main() {
    use postgres::schema::posts::dsl::posts;
    use postgres::schema::posts::title;

    let target = args().nth(1).expect("Expected a target to match against");
    let pattern = format!("%{}%", target);

    let connection = &mut establish_connection();
    let num_deleted = diesel::delete(posts.filter(title.like(pattern)))
        .execute(connection)
        .expect("Error deleting posts");

    println!("Deleted {} posts", num_deleted);
}