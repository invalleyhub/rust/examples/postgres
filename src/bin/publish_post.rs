use postgres::models::Post;
use postgres::establish_connection;

use std::env::args;

use diesel::QueryDsl;
use diesel::ExpressionMethods;
use diesel::RunQueryDsl;

fn main() {
    use postgres::schema::posts::dsl::{posts, published};

    let id = args()
        .nth(1)
        .expect("publish_post requires a post id")
        .parse::<i32>()
        .expect("Invalid ID");

    let connection = &mut establish_connection();

    let post = diesel::update(posts.find(id))
        .set(published.eq(true))
        .get_result::<Post>(connection)
        .unwrap();

    println!("Published post {}", post.title);
}