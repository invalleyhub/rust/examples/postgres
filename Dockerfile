FROM localhost:5000/rust:1.65.0.a96ab7f1

ARG DB_ADDR=""
ARG DB_USER=""
ARG DB_PASS=""

ENV DATABASE_URL=postgres://${DB_USER}:${DB_PASS}@${DB_ADDR}/diesel_demo

RUN sudo microdnf install libpq-devel -y

WORKDIR /app

EXPOSE 9091
